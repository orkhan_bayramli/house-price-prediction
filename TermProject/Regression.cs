﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using NumSharp;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearRegression;

namespace TermProject
{
    public class Regression
    {
        public static void Main()
        {

            // Format matrix output to console
            var formatProvider = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            formatProvider.TextInfo.ListSeparator = " ";

            Random random = new Random();
            
            double[,] data = FromCsv("/Users/orkhanbayramli/Desktop/flats.csv");
            int numberOfRows = 111;
            int numberOfColumns = 4;
            double[,] X = new double[numberOfRows, numberOfColumns];
            double[] Y = new double[numberOfRows];

            // Slice of a matrix without price
            for (int i = 0; i < numberOfRows; i++)
            {
                for (int j = 0; j < numberOfColumns; j++)
                {
                    X[i, j] = data[i, j];
                }
            }

            // Slice of a matrix contains only price
            for (int i = 0; i < numberOfRows; i++)
            {
                Y[i] = data[i, numberOfColumns];
            }

            Matrix<double> M = Matrix<double>.Build.DenseOfArray(X);
            Vector<double> V = Vector<double>.Build.DenseOfArray(Y);

            var res = M.TransposeThisAndMultiply(M).Cholesky().Solve(M.TransposeThisAndMultiply(V));

            Console.WriteLine(M.ToString("#0.00\t", formatProvider));
            Console.WriteLine(V.ToString("#0.00\t", formatProvider));
            Console.WriteLine(res.ToString("#0.00\t", formatProvider));
        }

        public static int[,] Transpose(int[,] matrix)
        {
            int w = matrix.GetLength(0);
            int h = matrix.GetLength(1);

            int[,] result = new int[h, w];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result[j, i] = matrix[i, j];
                }
            }

            return result;
        }

        public static void printMatrix(int[,] matrix)
        {
            int row = matrix.GetLength(0);
            int col = matrix.GetLength(1);

            Console.WriteLine("row: {0}\ncolumn: {1}\n", row, col);

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    Console.WriteLine("a[{0},{1}] = {2}", i, j, matrix[i, j]);
                }
            }
        }

        public static double[,] FromCsv(string path)
        {
            StreamReader sr = null;

            double[,] data = null;

            try
            {
                sr = new StreamReader(path);
                string line;

                ArrayList list = new ArrayList();

                while ((line = sr.ReadLine()) != null)
                {
                    list.Add(line);
                }

                data = new double[list.Count, 5];

                for (int i = 0; i < list.Count; i++)
                {
                    string res = (string)list[i];
                    string[] splittedRes = res.Split(',');

                    // I have 5 columns
                    for (int j = 0; j < 5; j++)
                    {
                        Console.WriteLine(splittedRes[j]);

                        switch (j)
                        {
                            case 0:
                                data[i, j] = double.Parse(splittedRes[j]);
                                break;
                            case 1:
                                data[i, j] = double.Parse(splittedRes[j]);
                                break;
                            case 2:
                                if (splittedRes[j].Equals("unrepaired"))
                                    data[i, j] = 1.0;
                                else if (splittedRes[j].Equals("good"))
                                    data[i, j] = 4.0;
                                else if (splittedRes[j].Equals("perfect"))
                                    data[i, j] = 5.0;
                                break;
                            case 3:
                                if (splittedRes[j].Contains("TRUE"))
                                    data[i, j] = 1;
                                else
                                    data[i, j] = 0;
                                break;
                            case 4:
                                data[i, j] = double.Parse(splittedRes[j]);
                                break;
                            default:
                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
            }

            return data;

        }

    }
}
