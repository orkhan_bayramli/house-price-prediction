﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

enum Condition
{
    unrepaired,
    good,
    perfect
}

class Flat
{
    public double area { get; set; }
    public int numberOfRooms { get; set; }
    public Condition condition { get; set; }
    public bool isDocumented { get; set; }
    public int price { get; set; }

    public void ToString()
    {
        Console.WriteLine(area + "\n" + numberOfRooms + "\n" + condition + "\n" + isDocumented + "\n" + price);
    }

    public Flat()
    {

    }

    public Flat(double area, int numberOfRooms, Condition condition, bool isDocumented, int price)
    {
        this.area = area;
        this.numberOfRooms = numberOfRooms;
        this.condition = condition;
        this.isDocumented = isDocumented;
        this.price = price;
    }

}


class MainClass
{
    static readonly HttpClient client = new HttpClient();
    static List<Flat> flats = new List<Flat>();

    //static async Task Main()
    //{
    //    await StartWebScrapingAsync();

    //    flats.Count();

    //    foreach (var item in flats)
    //    {
    //        item.ToString();
    //    }

    //    string filePath = "/Users/orkhanbayramli/Desktop/flats.csv";
    //    ToCsv(filePath, flats);


    //}


    private static async Task StartWebScrapingAsync()
    {
        string baseUrlForListOfFlats = "https://emlak.az/elanlar/?ann_type=3&announce_type=18880&property_type=1&selected_metros[]=10&sort_type=3&page=";
        int numberOfPages = 6;

        string baseUrlForFlat = "https://emlak.az";

        try
        {
            for (int i = 1; i <= numberOfPages; i++)
            {
                string stringHtml = await client.GetStringAsync(baseUrlForListOfFlats + i);

                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(stringHtml);

                List<HtmlNode> listOfFlats = htmlDocument.DocumentNode.SelectNodes("/html/body/div[1]/div[1]/div[3]")
                    .Descendants("div")
                    .Where(node => node.GetAttributeValue("class", "").Equals("ticket clearfix ")).ToList();

                foreach (var childNode in listOfFlats)
                {
                    string pathToFlat = childNode.SelectSingleNode("div/a")
                        .GetAttributeValue("href", "").ToString();

                    await GetFlatAsync(baseUrlForFlat + pathToFlat);
                }

            }

        }
        catch (HttpRequestException ex)
        {
            Console.WriteLine("\nException Caught while requesting Urls of Flats: {0}", ex);

        }
        catch (Exception ex)
        {
            Console.WriteLine("\n{0} Caught: {1}", ex.GetBaseException(), ex);
        }

    }


    private static async Task GetFlatAsync(string url)
    {
        try
        {
            Flat flat = new Flat();

            string stringHtml = await client.GetStringAsync(url);

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(stringHtml);

            string rawPrice = htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[2]/div[1]/div[3]/div[1]/span[1]")
                .GetDirectInnerText();
            int priceInThousands = int.Parse(Regex.Match(rawPrice, @"\d+").Value);
            flat.price = priceInThousands;


            List<HtmlNode> childNodesOfFlat = htmlDocument.DocumentNode.SelectNodes("/html/body/div[2]/div[1]/div[3]/dl")
                .Descendants("dd").ToList();

            if (childNodesOfFlat.Count == 0)
            {
                throw new NullReferenceException("The nodes of a flat is empty. Error occurred.");
            }


            foreach (var childNode in childNodesOfFlat)
            {

                string insideChildNodeTitle = childNode.FirstChild.GetDirectInnerText();
                string insideChildNode = childNode.GetDirectInnerText();


                if (insideChildNodeTitle.Contains("Sahə"))
                {
                    flat.area = double.Parse(Regex.Match(insideChildNode, @"\d+").Value);
                }
                else if (insideChildNodeTitle.Contains("Otaqların sayı"))
                {
                    flat.numberOfRooms = int.Parse(insideChildNode);
                }
                else if (insideChildNodeTitle.Contains("Təmiri"))
                {
                    if (insideChildNode.Contains("Əla"))
                    {
                        flat.condition = Condition.perfect;
                    }
                    else if (insideChildNode.Contains("Yaxşı"))
                    {
                        flat.condition = Condition.good;
                    }
                    else if (insideChildNode.Contains("Təmirsiz"))
                    {
                        flat.condition = Condition.unrepaired;
                    }
                }
                else if (insideChildNodeTitle.Contains("Sənəd"))
                {
                    if (insideChildNode.Contains("Çıxarış"))
                    {
                        flat.isDocumented = true;
                    }
                    else
                    {
                        flat.isDocumented = false;
                    }
                }

            }

            flats.Add(flat);
        }
        catch (HttpRequestException ex)
        {
            Console.WriteLine("\nException Caught while requesting HTML of a Flat: {0}", ex);
        }
        catch (Exception ex)
        {
            Console.WriteLine("\n{0} Caught: {1}", ex.GetBaseException(), ex);
        }
    }

    public static void ToCsv(string path, List<Flat> flats)
    {
        CreateAndDelete(path);

        StreamWriter sw = null;

        try
        {
            sw = new StreamWriter(path);
            foreach (var flat in flats)
            {
                sw.WriteLine("" + flat.area + "," + flat.numberOfRooms + "," + flat.condition + "," + flat.isDocumented + "," + flat.price);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        finally
        {
            if (sw != null)
            {
                sw.Close();
            }
        }
    }

    private static void CreateAndDelete(String path)
    {

        if (!File.Exists(path))
        {
            Console.WriteLine("File in path {0} does not exist. We are going to create it", path);
            File.Create(path);
        }
        else
        {
            Console.WriteLine("File in path {0} exists", path);
            File.Delete(path);
            Console.WriteLine("File in path {0} has been deleted", path);
            Console.WriteLine("Does file in path {0} still exist? {1}", path, File.Exists(path));
        }
    }
}